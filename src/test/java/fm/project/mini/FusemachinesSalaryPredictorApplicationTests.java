package fm.project.mini;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import fm.project.mini.controller.ManagerController;
import fm.project.mini.service.ManagerService;

@RunWith(SpringRunner.class)
@WebMvcTest(ManagerController.class)
public class FusemachinesSalaryPredictorApplicationTests {

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private ManagerService managerService;
	
	Manager manager = new Manager(100, "name","department","shift", 20000, 2);
	Manager newManager = new Manager(100, "newname","newdepartment","newshift", 30000, 3);

	@Test
	public void postTestException() throws Exception {
		Mockito.when(this.managerService.doesManagerExist(Mockito.any(Manager.class))).thenReturn(true);
		ObjectMapper objectMapper = new ObjectMapper();
		String exampleManagerJson = objectMapper.writeValueAsString(newManager);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/managers").accept(MediaType.APPLICATION_JSON).content(exampleManagerJson).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		Mockito.verify(managerService, Mockito.times(0)).addManager(Mockito.any(Manager.class));
		assertEquals(HttpStatus.CONFLICT.value(), response.getStatus());
	}
	
	
	@Test
	public void postTestNoException() throws Exception {
		Mockito.when(this.managerService.doesManagerExist(Mockito.any(Manager.class))).thenReturn(false);
		ObjectMapper objectMapper = new ObjectMapper();
		String exampleManagerJson = objectMapper.writeValueAsString(manager);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/managers").accept(MediaType.APPLICATION_JSON).content(exampleManagerJson).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		Mockito.verify(managerService, Mockito.times(1)).addManager(Mockito.any(Manager.class));
		assertEquals(HttpStatus.CREATED.value(), response.getStatus());
		assertEquals("http://localhost/managers/100", response.getHeader(HttpHeaders.LOCATION));
	}
	
	
	@Test
	public void getTest() throws Exception{
		Mockito.when(this.managerService.getManager(100)).thenReturn(manager);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/managers/{id}", manager.getId()).accept(MediaType.APPLICATION_JSON);
		MvcResult result = mvc.perform(requestBuilder).andReturn();
		ObjectMapper objectMapper = new ObjectMapper();
		String expected = objectMapper.writeValueAsString(manager);
//		String expected = "{id:100,name:name,department:department,shift:shift,salary:20000,noOfProjectsHandled:2}";
//		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
		assertThat(expected).isEqualTo(result.getResponse().getContentAsString());
		assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
	}
	
	@Test
	public void getAllTest() throws Exception{
		List<Manager> managers = new ArrayList<>();
		managers.add(manager);
		Mockito.when(this.managerService.getAllManagers()).thenReturn(managers);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/managers").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mvc.perform(requestBuilder).andReturn();
		ObjectMapper objectMapper = new ObjectMapper();
		String expected = objectMapper.writeValueAsString(managers);
		assertThat(expected).isEqualTo(result.getResponse().getContentAsString());
		assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
	}
	
	@Test
	public void putTestException() throws Exception {
		Mockito.when(this.managerService.getManager(100)).thenReturn(null);
		ObjectMapper objectMapper = new ObjectMapper();
		String exampleManagerJson = objectMapper.writeValueAsString(newManager);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/managers/100").accept(MediaType.APPLICATION_JSON).content(exampleManagerJson).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		Mockito.verify(managerService, Mockito.times(0)).updateManager(Mockito.anyInt(), Mockito.any(Manager.class));
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
	}
	
		
	@Test
	public void putTestNoException() throws Exception {
		Mockito.when(this.managerService.getManager(100)).thenReturn(manager);
		ObjectMapper objectMapper = new ObjectMapper();
		String exampleManagerJson = objectMapper.writeValueAsString(newManager);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/managers/100").accept(MediaType.APPLICATION_JSON).content(exampleManagerJson).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		String expected = objectMapper.writeValueAsString(newManager);
		Mockito.verify(managerService, Mockito.times(1)).updateManager(Mockito.anyInt(), Mockito.any(Manager.class));
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		assertThat(expected).isEqualTo(result.getResponse().getContentAsString());
	}
	
	@Test
	public void deleteTestException() throws Exception {
		Mockito.when(this.managerService.getManager(100)).thenReturn(null);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/managers/100").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		Mockito.verify(managerService, Mockito.times(0)).deleteManager(Mockito.anyInt());
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
	}

	@Test
	public void deleteTestNoException() throws Exception {
		Mockito.when(this.managerService.getManager(100)).thenReturn(manager);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/managers/100").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		Mockito.verify(managerService, Mockito.times(1)).deleteManager(Mockito.anyInt());
		assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatus());
	}
}
