package fm.project.mini;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import fm.project.mini.repository.ManagerRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE)
public class DataJPATest {

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private ManagerRepository managerRepository;
	
	private Manager mng = new Manager("name","department","shift", 20000, 2);
	
	@Before
	public void beforeTest() {
		this.entityManager.persist(mng);
	}
	@Test
	public void dataTest() throws Exception {
		
		Manager manager = this.managerRepository.findOne(mng.getId());
		assertThat(manager.getId()).isEqualTo(mng.getId());
        assertThat(manager.getName()).isEqualTo("name");
        assertThat(manager.getDepartment()).isEqualTo("department");
        assertThat(manager.getShift()).isEqualTo("shift");
        assertThat(manager.getSalary()).isEqualTo(20000);
        assertThat(manager.getNoOfProjectsHandled()).isEqualTo(2);
	}
	
	@After
	public void afterTest() {
		managerRepository.delete(mng);
	}
}
