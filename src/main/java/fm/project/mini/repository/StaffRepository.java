package fm.project.mini.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import fm.project.mini.Staff;

@NoRepositoryBean
public interface StaffRepository<T extends Staff> extends JpaRepository<T, Integer> {

}
