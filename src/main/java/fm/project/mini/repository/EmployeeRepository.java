package fm.project.mini.repository;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import fm.project.mini.Employee;

@Transactional
public interface EmployeeRepository extends StaffRepository<Employee> {
	public List<Employee> findByManagerId(int managerId);
}
