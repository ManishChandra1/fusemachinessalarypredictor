package fm.project.mini.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fm.project.mini.Manager;

@Transactional
public interface ManagerRepository extends StaffRepository<Manager> {

}
