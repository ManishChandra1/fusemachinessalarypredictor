package fm.project.mini;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Min;

@MappedSuperclass
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Staff {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private int id;
	
	@Column(nullable = false, length = 100)
	private String name;
	
	@Column(length = 50)
	private String department;
	
	@Column(length = 20)
	private String shift;
	
	@Min(0)
	private int salary;
	
	protected Staff() {
		
	}
	
	public Staff(String name, String department, String shift, int salary) {
		super();
		this.name = name;
		this.department = department;
		this.shift = shift;
		this.salary = salary;
	}
	
	public Staff(int id, String name, String department, String shift, int salary) {
		super();
		this.id = id;
		this.name = name;
		this.department = department;
		this.shift = shift;
		this.salary = salary;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getShift() {
		return shift;
	}

	public void setShift(String shift) {
		this.shift = shift;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}	
	
}
