package fm.project.mini.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import fm.project.mini.Employee;
import fm.project.mini.Manager;
import fm.project.mini.service.EmployeeService;
import fm.project.mini.util.CustomErrorType;

@RestController
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
		
	@RequestMapping(method=RequestMethod.POST, value="managers/{managerId}/employees")
	public ResponseEntity<?> addEmployee(@PathVariable int managerId, @RequestBody Employee employee, UriComponentsBuilder ucBuilder) {
		if (employeeService.doesEmployeeExist(employee)) {
            return new ResponseEntity(new CustomErrorType("Unable to create. An employee with id " + 
            employee.getId() + " already exists."),HttpStatus.CONFLICT);
        }
		employee.setSalaryIncrease(employeeService.test(employee.getAttendanceScore(), employee.getNoOfProjects(), employee.getSupervisorScore()));
		employee.setManager(new Manager(managerId,"","","",0,0));
		employeeService.addEmployee(employee);
		HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/managers/{managerId}/employees/{id}").buildAndExpand(managerId, employee.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}
	
	@RequestMapping("/managers/{managerId}/employees")
	public ResponseEntity<List<Employee>> getAllEmployees(@PathVariable int managerId) {
		List<Employee> employees = employeeService.getAllEmployees(managerId);
		if (employees.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
	}
	
	@RequestMapping("/managers/{managerId}/employees/{id}")
	public ResponseEntity<?> getEmployee(@PathVariable("id") int id) {
		Employee emp = employeeService.getEmployee(id);
		if (emp == null) {
            return new ResponseEntity(new CustomErrorType("Employee with id " + id 
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Employee>(emp, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="managers/{managerId}/employees/{id}")
	public ResponseEntity<?> updateEmployee(@RequestBody Employee employee, @PathVariable int id, @PathVariable int managerId) {
		Employee currentEmployee = employeeService.getEmployee(id);
		if (currentEmployee == null) {
            return new ResponseEntity(new CustomErrorType("Unable to upate. Employee with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
		employee.setSalaryIncrease(employeeService.test(employee.getAttendanceScore(), employee.getNoOfProjects(), employee.getSupervisorScore()));
		employee.setManager(new Manager(managerId,"","","",0,0));
		employeeService.updateEmployee(id, employee);
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="managers/{managerId}/employees/{id}")
	public ResponseEntity<?> deleteEmployee(@PathVariable int id) {
		Employee emp = employeeService.getEmployee(id);
		if (emp == null) {
            return new ResponseEntity(new CustomErrorType("Unable to delete. Employee with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
		employeeService.deleteEmployee(id);
		return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
	}
	
	@RequestMapping("/train")
	public void training() {
		employeeService.train();
		
	}
}
