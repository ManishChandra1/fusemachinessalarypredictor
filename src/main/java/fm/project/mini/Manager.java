package fm.project.mini;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Min;

@Entity
public class Manager extends Staff {

	@Min(0)
	private int noOfProjectsHandled;
	
	public int getNoOfProjectsHandled() {
		return noOfProjectsHandled;
	}

	public void setNoOfProjectsHandled(int noOfProjectsHandled) {
		this.noOfProjectsHandled = noOfProjectsHandled;
	}

	protected Manager() {
		
	}

	public Manager(String name, String department, String shift, int salary, int noOfProjectsHandled) {
		super(name, department, shift, salary);
		this.noOfProjectsHandled = noOfProjectsHandled;
	}
	
	public Manager(int id, String name, String department, String shift, int salary, int noOfProjectsHandled) {
		super(id, name, department, shift, salary);
		this.noOfProjectsHandled = noOfProjectsHandled;
	}
	
}
