package fm.project.mini.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.ejml.data.DMatrixRMaj;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fm.project.mini.Employee;
import fm.project.mini.repository.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	public void addEmployee(Employee employee) {
		employeeRepository.save(employee);
	}
	
	public List<Employee> getAllEmployees(int managerId) {
		List<Employee> employees = new ArrayList<>();
		this.employeeRepository.findByManagerId(managerId).forEach(employees::add);
		return employees;
	}
	
	public Employee getEmployee(int id) {
		return employeeRepository.findOne(id);
	}

	public void updateEmployee(int id, Employee employee) {
		employeeRepository.save(employee);
	}

	public void deleteEmployee(int id) {
		employeeRepository.delete(id);
	}

	public boolean doesEmployeeExist(Employee employee) {
		Employee emp = employeeRepository.findOne(employee.getId());
		if(emp != null) {
			return true;
		}
		return false;
	}
	
	public void train() {
		SimpleMatrix x = new SimpleMatrix(50,3+1);
		SimpleMatrix y = new SimpleMatrix(50,1);
		SimpleMatrix w = new SimpleMatrix(4,1);
		int i=0;
		for(Employee emp:employeeRepository.findAll()) {
			x.set(i,0,1);
			x.set(i,1,emp.getAttendanceScore());
			x.set(i,2,emp.getNoOfProjects());
			x.set(i,3,emp.getSupervisorScore());
			y.set(i,0,emp.getSalaryIncrease());
			i++;
		}
//		x.print();
//		y.print();
		SimpleMatrix xt = x.transpose(); 
		xt.print();
		w = (xt.mult(x)).invert().mult(xt).mult(y);
		w.print();
		try {
			w.saveToFileBinary("weights");;
		}
		catch (IOException e) {
	        throw new RuntimeException(e);
		}
		
	}
	
	public int test(int attendance, int projects, int supervisor) {
		SimpleMatrix x = new SimpleMatrix(1,4);
		x.set(0,0,1);
		x.set(0,1,attendance);
		x.set(0,2,projects);
		x.set(0,3,supervisor);
		try {
			SimpleMatrix w = SimpleMatrix.loadBinary("weights");
	        w.print();
	        return (int)(x.mult(w)).get(0);
	    } 
		catch (IOException e) {
	        throw new RuntimeException(e);
		}	
	}
}

