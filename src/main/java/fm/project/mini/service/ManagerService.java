package fm.project.mini.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fm.project.mini.Manager;
import fm.project.mini.repository.ManagerRepository;

@Service
public class ManagerService {

	@Autowired
	private ManagerRepository managerRepository;
	
	public void addManager(Manager manager) {
		managerRepository.save(manager);
	}
	
	public List<Manager> getAllManagers() {
		List<Manager> managers = new ArrayList<>();
		this.managerRepository.findAll().forEach(managers::add);
		return managers;
	}
	
	public Manager getManager(int id) {
		return managerRepository.findOne(id);
	}

	public void updateManager(int id, Manager manager) {
		managerRepository.save(manager);
	}

	public void deleteManager(int id) {
		managerRepository.delete(id);
	}

	public boolean doesManagerExist(Manager manager) {
		Manager mng = managerRepository.findOne(manager.getId());
		if(mng != null) {
			return true;
		}
		return false;
	}
	
	
}
