package fm.project.mini;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
public class Employee extends Staff {

	@Min(0)
	private int noOfProjects;
	
	@Min(0)
	@Max(100)
	private int supervisorScore;
	
	@Min(0)
	@Max(100)
	private int attendanceScore;
	
	@OneToOne
	private Manager manager;
	
	private int salaryIncrease;

	public int getSalaryIncrease() {
		return salaryIncrease;
	}

	public void setSalaryIncrease(int salaryIncrease) {
		this.salaryIncrease = salaryIncrease;
	}

	protected Employee() {
		
	}

	public Employee(String name, String department, String shift, int salary, int noOfProjects, int supervisorScore, int attendanceScore, Manager manager) {
		super(name, department, shift, salary);
		this.noOfProjects = noOfProjects;
		this.supervisorScore = supervisorScore;
		this.attendanceScore = attendanceScore;
		this.manager = manager;
	}

	public Employee(int id, String name, String department, String shift, int salary, int noOfProjects, int supervisorScore, int attendanceScore, Manager manager) {
		super(id, name, department, shift, salary);
		this.noOfProjects = noOfProjects;
		this.supervisorScore = supervisorScore;
		this.attendanceScore = attendanceScore;
		this.manager = manager;
	}
	
	public int getNoOfProjects() {
		return noOfProjects;
	}

	public void setNoOfProjects(int noOfProjects) {
		this.noOfProjects = noOfProjects;
	}

	public int getSupervisorScore() {
		return supervisorScore;
	}

	public void setSupervisorScore(int supervisorScore) {
		this.supervisorScore = supervisorScore;
	}

	public int getAttendanceScore() {
		return attendanceScore;
	}

	public void setAttendanceScore(int attendanceScore) {
		this.attendanceScore = attendanceScore;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}
}
