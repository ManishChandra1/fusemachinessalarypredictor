package fm.project.mini;

import org.ejml.data.DMatrixRMaj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import fm.project.mini.service.EmployeeService;

@SpringBootApplication
public class FusemachinesSalaryPredictorApplication {
	
	public static void main(String[] args) {
		
		
		SpringApplication.run(FusemachinesSalaryPredictorApplication.class, args);
	}
}
